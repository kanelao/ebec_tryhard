import 'package:ebec_tryhard/widgets/timeItemEntry.dart';
import 'package:flutter/material.dart';

class MainPage extends StatelessWidget {
  final int numberOfReservationsDone;
  final int maxReservations;
  final List<Widget> timeListItems = <Widget>[
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: true,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: true,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: true,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
    TimeItemEntry(
      endingHour: DateTime.now(),
      beginingHour: DateTime.now().subtract(Duration(minutes: 30)),
      available: false,
    ),
  ];

  MainPage(
      {@required this.numberOfReservationsDone,
      @required this.maxReservations});

  Widget informationBar(){
    return Container(
            padding: EdgeInsets.all(8),
            height: 50,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Text('Reservas Feitas: $numberOfReservationsDone',
                    style: TextStyle(fontSize: 20)),
                Text('Máximo de Reservas: $maxReservations',
                    style: TextStyle(fontSize: 20)),
              ],
            ),
          );
  }

  Widget timeEntriesList(){
    return Expanded(
            child: ListView(
              padding: EdgeInsets.all(0),
              children: timeListItems,
            ),
          );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(top: 40),
        child: Column(children: [
          informationBar(),
          timeEntriesList(),
        ]));
  }
}
