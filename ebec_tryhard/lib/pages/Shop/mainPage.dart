import 'package:ebec_tryhard/pages/Shop/itemList.dart';
import 'package:ebec_tryhard/pages/Shop/shopList.dart';
import 'package:flutter/material.dart';

class MainPage extends StatefulWidget {
  final List<Widget> pages = [ShopList(), ItemList()];

  MainPage();

  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage>
    with SingleTickerProviderStateMixin {
  int currentPage;
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  Widget appBar(){
    return TabBar(
          controller: _tabController,
          tabs: <Widget>[
            Tab(text: 'Mercado'),
            Tab(text: 'Inventário'),
          ],
        );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 40), // TODO: tirar no final
      child: Scaffold(
        appBar: appBar(),
        body: TabBarView(
          controller: _tabController,
          children: widget.pages,
        ),
      ),
    );
  }
}
