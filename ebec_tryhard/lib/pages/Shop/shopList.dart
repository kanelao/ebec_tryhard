import 'package:ebec_tryhard/widgets/shopItemEntry.dart';
import 'package:flutter/material.dart';

class ShopList extends StatelessWidget {
  final int numberOfItems = 1;
  final int total = 0;
  final List<Widget> shopGridItems = [
              ShopItemEntry(3, 5),
              ShopItemEntry(2, 6),
              ShopItemEntry(3, 5),
              ShopItemEntry(2, 6),
              ShopItemEntry(3, 5),
              ShopItemEntry(2, 6),
              ShopItemEntry(3, 5),
              ShopItemEntry(2, 6),
              ShopItemEntry(3, 5),
              ShopItemEntry(2, 6),
              ShopItemEntry(3, 5),
              ShopItemEntry(2, 6),
              ShopItemEntry(3, 5),
              ShopItemEntry(2, 6),
              ShopItemEntry(3, 5),
              ShopItemEntry(2, 6),
              ShopItemEntry(3, 5),
              ShopItemEntry(2, 6),
            ];

  ShopList();

  Widget informationBar() {
    return Container(
      padding: EdgeInsets.all(8),
      height: 50,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Text('Number of Items: $numberOfItems',
              style: TextStyle(fontSize: 20)),
          Text('Total Cost: $total', style: TextStyle(fontSize: 20)),
          IconButton(
            icon: Icon(Icons.check),
            onPressed: numberOfItems <= 0 ? null : () {},
          ),
        ],
      ),
    );
  }

  Widget shopItemsGrid(){
    return Expanded(
          child: GridView.count(
            crossAxisCount: 2,
            mainAxisSpacing: 0,
            childAspectRatio: 0.9,
            children: shopGridItems,
          ),
        );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        informationBar(),
        shopItemsGrid(),
      ],
    );
  }
}
