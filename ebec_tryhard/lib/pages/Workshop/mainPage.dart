import 'package:ebec_tryhard/pages/Shop/itemList.dart';
import 'package:ebec_tryhard/pages/Shop/shopList.dart';
import 'package:ebec_tryhard/widgets/officeItemEntry.dart';
import 'package:flutter/material.dart';

class MainPage extends StatelessWidget {
  final List<Widget> workshopListItems = <Widget>[
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
    WorkshopItemEntry(
      quantity: 2,
      name: 'Item',
    ),
  ];

  Widget informationBar() {
    return Container(
      padding: EdgeInsets.all(8),
      height: 40,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          Text('Ocupação: Livre', style: TextStyle(fontSize: 20)),
          Text('Loja: Aberta', style: TextStyle(fontSize: 20)),
        ],
      ),
    );
  }

  Widget workshopItemList() {
    return Expanded(
      child: GridView.count(
        crossAxisCount: 2,
        padding: EdgeInsets.all(0),
        children: workshopListItems,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 40), // TODO: tirar no final
      child: Column(
        children: [
          informationBar(),
          workshopItemList(),
        ],
      ),
    );
  }
}
