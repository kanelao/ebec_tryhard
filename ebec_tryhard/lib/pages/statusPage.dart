import 'dart:math';

import 'package:flutter/material.dart';

typedef ChangeBudget = void Function(double budget);

class StatusPage extends StatelessWidget {
  final double budget;
  final double used;
  final TextEditingController newBudget = TextEditingController();

  StatusPage({
    @required this.budget,
    @required this.used,
  });

  Widget _balance(double remaining) {
    Color textColor = remaining < 0 ? Colors.redAccent : Colors.greenAccent;
    return Column(
      children: [
        Text(
          '$remaining',
          style: TextStyle(
              color: textColor, fontWeight: FontWeight.bold, fontSize: 23),
        ),
        Container(
          width: 70,
          height: 4,
          color: remaining < 0 ? Colors.redAccent : Colors.greenAccent,
          margin: EdgeInsets.only(top: 5, bottom: 5),
        ),
        Text(
          'Balance',
          style: TextStyle(fontWeight: FontWeight.bold, fontSize: 23),
        ),
      ],
    );
  }

  Widget _budget() {
    return Column(
      children: [
        Text('$budget',
            style: TextStyle(
                color: Colors.grey[300], fontWeight: FontWeight.bold)),
        Container(
          width: 50,
          height: 2,
          color: Colors.grey[300],
          margin: EdgeInsets.only(top: 5, bottom: 5),
        ),
        Text(
          'Budget',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ],
    );
  }

  Widget _used() {
    return Column(
      children: [
        Text('$used',
            style: TextStyle(
                color: Colors.tealAccent, fontWeight: FontWeight.bold)),
        Container(
          width: 50,
          height: 2,
          color: Colors.tealAccent,
          margin: EdgeInsets.only(top: 5, bottom: 5),
        ),
        Text(
          'Total',
          style: TextStyle(fontWeight: FontWeight.bold),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    double remaining = budget - used;
    return Stack(children: [
      Container(
        height: 80,
        child: AppBar(
          title: Text('Status'),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
      ),
      Center(
        child: CustomPaint(
          painter: StatusPainter(
              value1: budget, value: used, screen: MediaQuery.of(context).size),
        ),
      ),
      Column(mainAxisAlignment: MainAxisAlignment.center, children: [
        Container(
            margin: EdgeInsets.all(5),
            padding: EdgeInsets.symmetric(vertical: 8),
            child: _balance(remaining)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _budget(),
            SizedBox(
              width: 10,
            ),
            _used(),
          ],
        ),
      ]),
    ]);
  }
}

class StatusPainter extends CustomPainter {
  final double value1;
  final double value;
  final Color color;
  Size screen;

  StatusPainter(
      {@required this.value, @required this.value1, this.screen, this.color});

  Paint colorPaint(Color color) {
    return Paint()
      ..color = color
      ..strokeWidth = 15.0
      ..style = PaintingStyle.stroke
      ..strokeCap = StrokeCap.round;
  }

  bool firstValueIsBiggerThanSecond(double firstValue, double secondValue) {
    return firstValue > secondValue;
  }

  void drawArcs(Canvas canvas, Rect rect, Paint firstPaint, Paint secondPaint, double percentage) {
    double total = 2 * pi - 0.3;
    canvas.drawArc(rect, 0, total, false, firstPaint);
    canvas.drawArc(rect, 0, total * percentage, false, secondPaint);
  }

  Rect screenRectangle(){
    screen = screen.height == 0 ? Size(100, 100) : screen;

    double square =
        screen.width < screen.height ? screen.width / 4 : screen.height / 4;
    return Rect.fromLTRB(-square, -square, square, square);
  }

  @override
  void paint(Canvas canvas, Size size) {
    Paint teal = colorPaint(Colors.tealAccent);
    Paint grey = colorPaint(Colors.grey[300]);
    Rect rect = screenRectangle();

    if (firstValueIsBiggerThanSecond(value1, value))
      drawArcs(canvas, rect, grey, teal, value/value1);
    else
      drawArcs(canvas, rect, teal, grey, value1/value);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}
