import 'package:ebec_tryhard/pages/Workshop/mainPage.dart' as workshop;
import 'package:ebec_tryhard/pages/Shop/mainPage.dart' as shop;
import 'package:ebec_tryhard/pages/TestBase/MainPage.dart' as test;
import 'package:ebec_tryhard/pages/statusPage.dart';
import 'package:ebec_tryhard/widgets/shopItemEntry.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      home: BottomBar(),
    );
  }
}

class BottomBar extends StatefulWidget {

  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  int _currentTab = 0;
  Widget callPage(int index) {
    switch (index) {
      case 0:
        return shop.MainPage();
      case 1:
        return workshop.MainPage();
      case 2:
        return test.MainPage(maxReservations: 3,numberOfReservationsDone: 1,);
      default:
        return CircularProgressIndicator();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: StatusPage(
          budget: 300,
          used: 400,
        ),
      ),
      body: callPage(_currentTab),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _currentTab,
        onTap: (int pressed) {
          setState(() {
            _currentTab = pressed;
          });
        },
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            title: Text('Loja'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.build),
            title: Text('Oficina'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.calendar_today),
            title: Text('Testes'),
          )
        ],
      ),
    );
  }
}
