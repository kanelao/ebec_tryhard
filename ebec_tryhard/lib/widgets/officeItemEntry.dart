import 'package:flutter/material.dart';

class WorkshopItemEntry extends StatelessWidget {
  final int quantity;
  final String name;

  WorkshopItemEntry({@required this.quantity, @required this.name});

  Widget image() {
    return Container(
      margin: EdgeInsets.only(bottom: 8),
      child: Image.network(
        'https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fhttp2.mlstatic.com%2Fdinossauro-6-display-de-mesa-D_NQ_NP_574211-MLB20515723051_122015-F.jpg&f=1',
        width: 180,
        height: 140,
        alignment: Alignment.center,
        fit: BoxFit.cover,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            width: 200,
            height: 210,
            child: Card(
              elevation: 2.0,
              child: Container(
                padding: EdgeInsets.all(8),
                child: Column(
                  children: <Widget>[
                    image(),
                    Text('$name', style: TextStyle(fontSize: 20)),
                    Text('Quantidade: $quantity'),
                  ],
                ),
              ),
            ),
          ),
        ]);
  }
}
