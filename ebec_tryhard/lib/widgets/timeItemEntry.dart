import 'package:flutter/material.dart';

class TimeItemEntry extends StatelessWidget {
  final DateTime beginingHour;
  final DateTime endingHour;
  final bool available;

  TimeItemEntry(
      {@required this.beginingHour,
      @required this.endingHour,
      @required this.available});

  static String dateTimeToFormatedDate(DateTime date) {
    return "${date.hour}:${date.minute}";
  }

  Widget lineWithCircleEdges() {
    return Column(
      children: <Widget>[
        Container(
            height: 4,
            width: 4,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2),
                color: Colors.tealAccent)),
        Container(height: 15, width: 2, color: Colors.tealAccent),
        Container(
            height: 4,
            width: 4,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(2),
                color: Colors.tealAccent)),
      ],
    );
  }

  Widget timeDisplay() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Text(dateTimeToFormatedDate(beginingHour),
            style: TextStyle(color: Colors.grey[300])),
        lineWithCircleEdges(),
        Text(dateTimeToFormatedDate(endingHour),
            style: TextStyle(color: Colors.grey[300])),
      ],
    );
  }

  Widget actionButton() {
    return IconButton(
      onPressed: () {},
      icon: Icon(
        available ? Icons.check : Icons.remove, //TODO
        color: available ? Colors.tealAccent : Colors.grey[300],
      ),
    );
  }

  Widget informationText() {
    return Text(
      available ? 'Available' : 'Unavailable',
      style: TextStyle(color: Colors.grey[300]),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10),
        ),
        padding: EdgeInsets.all(8),
        height: 70,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            timeDisplay(),
            informationText(),
            actionButton(),
          ],
        ),
      ),
    );
  }
}
