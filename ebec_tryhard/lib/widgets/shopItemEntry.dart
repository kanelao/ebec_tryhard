import 'package:flutter/material.dart';

class ShopItemEntry extends StatefulWidget {
  final int max;
  final int price;

  ShopItemEntry(this.max, this.price);

  _ShopItemEntryState createState() => _ShopItemEntryState();
}

class _ShopItemEntryState extends State<ShopItemEntry> {
  int current = 0;

  Widget image() {
    return Container(
      margin: EdgeInsets.only(bottom: 8),
      child: Image.network(
        'https://proxy.duckduckgo.com/iu/?u=https%3A%2F%2Fhttp2.mlstatic.com%2Fdinossauro-6-display-de-mesa-D_NQ_NP_574211-MLB20515723051_122015-F.jpg&f=1',
        width: 180,
        height: 140,
        alignment: Alignment.center,
        fit: BoxFit.cover,
      ),
    );
  }

  Widget informationRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Text('Unidade: ${widget.price}'),
        SizedBox(width: 10),
        Text('Total: ${current * widget.price}')
      ],
    );
  }

  Widget decrementButton() {
    return Container(
      height: 30,
      child: IconButton(
        onPressed: current == 0
            ? null
            : () {
                setState(() {
                  current--;
                });
              },
        icon: Icon(Icons.remove),
      ),
    );
  }

  Widget increaseButton() {
    return Container(
      height: 30,
      child: IconButton(
        onPressed: current == widget.max
            ? null
            : () {
                setState(() {
                  current++;
                });
              },
        icon: Icon(Icons.add),
      ),
    );
  }

  Widget purchaseInformation() {
    return Container(
      height: 35,
      alignment: Alignment.bottomCenter,
      child: Text(
        '$current/${widget.max}',
        style: TextStyle(fontSize: 24),
      ),
    );
  }

  Widget purchaseRow() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        decrementButton(),
        purchaseInformation(),
        increaseButton(),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            width: 200,
            height: 230,
            child: Card(
              elevation: 2.0,
              child: Container(
                padding: EdgeInsets.all(8),
                child: Column(
                  children: <Widget>[
                    image(),
                    informationRow(),
                    purchaseRow(),
                  ],
                ),
              ),
            ),
          ),
        ]);
  }
}
